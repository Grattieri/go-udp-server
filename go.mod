module gitlab.com/Grattieri/go-udp-server

go 1.14

require (
	github.com/go-bindata/go-bindata v3.1.2+incompatible // indirect
	github.com/gorilla/mux v1.7.4
	github.com/rs/cors v1.7.0
)
